FROM node:9.7-alpine

ENV ACCESS_LOG=/var/log/raspi/access_main.log
ENV ERROR_LOG=/var/log/raspi/error_main.log
ENV HTTP_PORT=80
VOLUME ["/var/log/raspi"]

WORKDIR /opt/raspi
RUN mkdir -p /opt/raspi

COPY . .
RUN \
 apk add --no-cache --virtual .build-deps musl=1.1.24-r0 musl-dev g++ python2 make --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
 npm install --production --no-cache && \
 apk --purge del .build-deps

RUN chmod +x /opt/raspi/start.sh

EXPOSE 80
ENTRYPOINT ["/opt/raspi/start.sh"]
