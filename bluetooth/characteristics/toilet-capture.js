var util = require('util');
var bleno = require('@abandonware/bleno');

function ToiletCaptureCharacteristic(toilet) {
    bleno.Characteristic.call(this, {
        uuid: '1ec7fc953a0d4f8eb436f6d5f10d316a',
        properties: ['read', 'write'],
        descriptors: [
            new bleno.Descriptor({
                uuid: '9a31402985cc4b28bfc739d13fe4b65a',
                value: 'Captures image of the toilet and encodes it.'
            })
        ]
    });

    this.toilet = toilet;
}

util.inherits(ToiletCaptureCharacteristic, bleno.Characteristic);

ToiletCaptureCharacteristic.prototype.onReadRequest = async function (offset, callback) {
    if (offset) {
        callback(this.RESULT_ATTR_NOT_LONG, null);
    } else {
        try {
            callback(this.RESULT_SUCCESS, await this.toilet.captureReadFrom());
        } catch (err) {
            console.log("Read error: " + err);
            callback(this.RESULT_UNLIKELY_ERROR, null);
        }
    }
};

module.exports = ToiletCaptureCharacteristic;
