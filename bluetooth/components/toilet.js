const { execSync } = require('child_process');
const fs = require('fs');
const util = require('util');
const events = require('events');
var lzma = require('lzma-native');

function Toilet() {
    events.EventEmitter.call(this);
    this.demo = false; // if true, only do a simulation (not calling real APIs)
    this.clientAddress = null;
    this.readBuffer = Buffer.from('');
    this.writeBuffer = Buffer.from('');
    this.instanceLocked = ''; // to prevent multiple requests at once
    this.LEDStatus = false;
}

util.inherits(Toilet, events.EventEmitter);

Toilet.prototype.lock = function (content) {
    // TODO: things when locking the instance from other requests
    this.instanceLocked = content;
}

Toilet.prototype.unlock = function () {
    // TODO: things when unlocking the instance from other requests
    this.instanceLocked = '';
}

Toilet.prototype.switchLED = function (status) {
    // TODO: turn on LED if status is true
    if (status) {
        console.log('Turning on LED via GPIO.');
        if (!this.demo) {
            execSync('echo "1" > /sys/class/gpio/gpio${LED_GPIO_PIN}/value');
        }
    } else {
        console.log('Turning off LED via GPIO.');
        if (!this.demo) {
            execSync('echo "0" > /sys/class/gpio/gpio${LED_GPIO_PIN}/value');
        }
    }
    this.LEDStatus = status;
};

Toilet.prototype.captureCamera = async function () {
    if (!this.demo) {
        console.log('Capturing image and will save it to captured.jpg.');
        execSync('raspistill -n -w 125 -h 125 -o captured.jpg');
        var input = fs.readFileSync('captured.jpg');
    } else {
        console.log('Capturing image (in demo mode, will read captured-demo.jpg).');
        var input = fs.readFileSync('captured-demo.jpg');
    }
    console.log('Encoding (compressing) image with LZMA.');
    return await lzma.compress(input);
}

Toilet.prototype.sliceReadBuffer = function () {
    var value = this.readBuffer.slice(0, 22);
    this.readBuffer = this.readBuffer.slice(22);
    return value;
}

Toilet.prototype.captureReadFromInit = async function () {
    this.switchLED(true);
    this.readBuffer = await this.captureCamera();
    this.switchLED(false);
}

Toilet.prototype.captureReadFrom = async function () {
    // Method that specifically made to handle "capture" read requests
    if (this.instanceLocked === 'read') {
        if (this.readBuffer.length > 0) {
            return this.sliceReadBuffer();
        } else {
            this.unlock();
            this.readBuffer = Buffer.from('');
            return this.readBuffer;
        }
    } else if (this.instanceLocked !== '') {
        throw 'Other operations has not completed yet.';
    } else {
        this.lock('read');
        await this.captureReadFromInit();
        // Return file size
        return Buffer.from('##' + this.readBuffer.length + '##');
    }
}

module.exports.Toilet = Toilet;
