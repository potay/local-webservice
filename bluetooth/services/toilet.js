var util = require('util');
var bleno = require('@abandonware/bleno');
var ToiletCaptureCharacteristic = require('../characteristics/toilet-capture');

function ToiletService(toilet) {
    bleno.PrimaryService.call(this, {
        uuid: 'a42242eda83942daa89d9bc41cbb8c6d',
        characteristics: [
            new ToiletCaptureCharacteristic(toilet)
        ]
    });
}

util.inherits(ToiletService, bleno.PrimaryService);

module.exports = ToiletService;
