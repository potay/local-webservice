#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export LED_GPIO_PIN=20
export NODE_ENV=production
echo "$LED_GPIO_PIN" > /sys/class/gpio/export || true
echo "out" > /sys/class/gpio/gpio$LED_GPIO_PIN/direction || true
node $DIR/bin.js
