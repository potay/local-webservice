module.exports = function(demo) {
    var toilet = require('./bluetooth/components/toilet');
    var bleno = require('@abandonware/bleno');
    var ToiletService = require('./bluetooth/services/toilet');

    var name = 'ToT.bio Smart Toilet';
    var toiletInstance = new toilet.Toilet();
    toiletInstance.demo = demo;
    var toiletService = new ToiletService(toiletInstance);

    bleno.on('stateChange', function (state) {
        if (state === 'poweredOn') {
            bleno.startAdvertising(name, [toiletService.uuid], function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            bleno.stopAdvertising();
        }
    });

    bleno.on('accept', function (clientAddress) {
        if (toiletInstance.clientAddress !== null) {
            bleno.disconnect();
            console.log("Another device is already connected.");
        } else {
            toiletInstance.clientAddress = clientAddress;
        }
    });

    bleno.on('disconnect', function (clientAddress) {
        if (toiletInstance.clientAddress === clientAddress || toiletInstance.clientAddress === null) {
            toiletInstance.clientAddress = null;
            toiletInstance.unlock();
            toiletInstance.readBuffer = Buffer.from('');
            toiletInstance.writeBuffer = Buffer.from('');
        }
    });

    bleno.on('advertisingStart', function (err) {
        if (!err) {
            console.log('Advertising...');
            bleno.setServices([
                toiletService
            ]);
        }
    });
}
