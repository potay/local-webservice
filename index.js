module.exports = function() {
    const express = require('express');
    const app = express();
    const toilet = require('./bluetooth/components/toilet');
    const lzma = require('lzma-native');

    const HTTP_PORT = process.env.HTTP_PORT || 3000;

    app.use(express.static(__dirname + '/public'));
    app.use(express.json({limit: '20mb'}));

    app.get('/demo/', async (_, res) => {
        // simulates device workflow
        const toiletInstance = new toilet.Toilet();
        toiletInstance.demo = true;
        const initPattern = /^##\d{1,18}##$/g
        let result = Buffer.from('');
        let chunk = Buffer.from('');
        do {
            chunk = await toiletInstance.captureReadFrom();
            if (result.length === 0 && initPattern.test(chunk.toString())) {
                console.log("File length: " + chunk.toString().replace('##', ''));
            } else {
                result = Buffer.concat([result, chunk]);
            }
        } while (chunk.toString() !== '');
        res.contentType('application/octet-stream');
        res.set('Content-Disposition', 'attachment; filename=encoded-data.bin');
        res.status(200).send(result);
    });

    app.post('/test-encode/', (req, res) => {
        try {
            lzma.compress(Buffer.from(req.body.image_base64, 'base64'), function(result) {
                res.contentType('application/octet-stream');
                res.set('Content-Disposition', 'attachment; filename=encoded-data.bin');
                res.status(200).send(result);
            });
        } catch (err) {
            console.log(err);
            res.json({"success": false, "message": err});
        }
    });

    app.listen(HTTP_PORT, () => {
        console.log(`HTTP server listening at http://localhost:${HTTP_PORT}`)
    });
}
