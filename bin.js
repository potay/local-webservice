// Program only accepts two argument: demo OR web
if (process.argv[2] === 'demo') {
    console.log('Welcome to tot.bio local (peripheral) software demo.');
    console.log('This mode simulates what will the software inside tot.bio peripheral do using Web Bluetooth.');
    console.log('This mode serves as Web Bluetooth server, while simulating the GPIO and Camera.');
    console.log('Please make sure that you have followed the Software Instruction, and your Bluetooth adapter are compatible.');
    const peripheral = require('./peripheral');
    peripheral(true);
} else if (process.argv[2] === 'web') {
    console.log('Welcome to tot.bio local (peripheral) software demo, the web version.');
    console.log('This mode simulates what will the software inside tot.bio peripheral do using web server.');
    console.log('This mode serves as an ordinary web server, simulating Web Bluetooth flow.');
    console.log('This mode is intended for devices that are not compatible to serve as Web Bluetooth server, if your device are compatible according to Software Instructions, you can use "demo" mode instead.');
    const index = require('./index');
    index();
} else {
    console.log('Welcome to tot.bio local (peripheral) software.');
    console.log('This mode can only be run inside tot.bio peripheral. Do not run it outside the peripheral.');
    console.log('If you are running this outside of the peripheral, please use "demo" mode instead.');
    console.log('Before you run this mode, make sure you have switched on the GPIO, and put the pin number as environment variable at "LED_GPIO_PIN".')
    console.log('GPIO PIN: ' + process.env.LED_GPIO_PIN);
    const peripheral = require('./peripheral');
    peripheral(false);
}
